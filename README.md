CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

The Quick Search API Clone is a Drupal module that allows you to quickly clone an existing search API index and its dependencies, 
like fields, facets, and processors. It also lets you customize the settings of the newly created index, 
such as the index name, server, and filters.


REQUIREMENTS
------------

This module requires the following modules to be installed and enabled:

* Search API - https://www.drupal.org/project/search_api


INSTALLATION
------------

Install the Quick Search API Clone module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Go to the Configuration > Search API page.
    2. Click the "Clone" button next to the search API index you want to clone.
    3. Enter the new index name, and adjust the settings as needed.
    4. Click "Clone" to create the new index.


MAINTAINERS
-----------

* Hatim Habbous (hhabbous) - https://www.drupal.org/u/hhabbous