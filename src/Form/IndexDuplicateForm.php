<?php

namespace Drupal\quick_search_index_clone\Form;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\search_api\Entity\Index;

/**
 * Class IndexDuplicateForm.
 *
 * @package Drupal\quick_search_index_clone\Form
 */
class IndexDuplicateForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quick_search_index_clone_index_duplicate_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Index $search_api_index = NULL) {
    $form['#title'] = $this->t('Duplicate of index "@label"', ['@label' => $search_api_index->label()]);

    $form['search_api_index'] = [
      '#type' => 'value',
      '#value' => $search_api_index,
    ];

    // Add a textfield for the index name.
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Index name'),
      '#description' => $this->t('Enter the displayed name for the index.'),
      '#default_value' => $this->t('Duplicate of @label', ['@label' => $search_api_index->label()]),
      '#required' => TRUE,
    ];

    // Add a machine name field for the index ID.
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine-readable name'),
      '#machine_name' => [
        'source' => ['name'],
        'exists' => '\Drupal\search_api\Entity\Index::load',
      ],
      '#maxlength' => 128,
      '#required' => TRUE,
    ];

    // Add a textfield for the index description.
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Enter a description for the index.'),
    ];

    // Add a checkbox to enable the index.
    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Only enabled indexes can be used for indexing and searching. This setting will only take effect if the selected server is also enabled.'),
      '#default_value' => TRUE,
    ];

    // Add actions to the form.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button to duplicate the index.
    $form['actions']['duplicate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Duplicate'),
      '#button_type' => 'primary',
    ];

    // Add a cancel button to go back to the search API overview page.
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('search_api.overview'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, Index $search_api_index = NULL) {
    $id = $form_state->getValue('id');
    $name = $form_state->getValue('name');
    $description = $form_state->getValue('description');
    $status = $form_state->getValue('status');

    // Get the Search API index from the form state.
    $search_api_index = $form_state->getValue('search_api_index');

    try {
      // Create a new index entity and set its ID and name.
      $new_index = $search_api_index->createDuplicate()
        ->set('id', $id)
        ->set('name', $name)
        ->set('description', $description)
        ->set('status', $status);

      // Save the new index entity.
      $new_index->save();

      // Redirect to the new index's edit form.
      $form_state->setRedirect('entity.search_api_index.edit_form', ['search_api_index' => $new_index->id()]);

      // Display a success message.
      $this->messenger()->addStatus(
        $this->t('The index "@name" has been duplicated as "@new_name".', ['@name' => $search_api_index->label(), '@new_name' => $new_index->label()]));
    }
    catch (EntityStorageException $e) {
      // Display an error message,
      // if there was an error during the saving process.
      $this->messenger()->addError(
        $this->t('An error occurred while saving the index: @error', ['@error' => $e->getMessage()]));
    }
    catch (\Exception $exception) {
      // Display an error message for other types of exceptions.
      $this->messenger()->addError($this->t('An error occurred: @error', ['@error' => $exception->getMessage()]));
    }
  }

}
